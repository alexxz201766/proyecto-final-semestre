//
//  main.cpp
//  proyectofinish
//
//  Created by ALEJANDRO TOLENTINO on 21/12/20.
//

#include <iostream>
#include<iomanip>
#include <unistd.h>

using namespace std;

class elevador
{
    int actual;
    int piso,i;
    
    
public:
    elevador();
    void ubicacion();
    void direccion();
    bool confirmacion();
};

elevador::elevador(){
    actual=1;
    piso=1;
    i=0;
}
bool elevador::confirmacion(){

    if(((i+1)==piso)||((i-1)==piso)){
        
        cout<<"\n\n\t\tA LLEGADO A SU DESTINO :) "<<endl;
        return true;
    }
    else{
        cout<<"Error piso no disponible"<<endl;
        return false;
    }
  }
void elevador::ubicacion(){
    cout<<"\n\nPiso Actual : " <<"[ "<<actual<< " ]"<<endl<<"\t\t";
}
void elevador::direccion(){
    
    ubicacion();
    do{
        cout<<"--------A que piso quiere ir : "<<endl;

        cin>>piso;
        if (piso>=16){
            cout<<"error, el edificio solo cuenta con 15 pisos"<<endl;
        }
        if((piso<1)||(piso>15)){
            system("cls");
        }
    }
        while((piso<1)||(piso>15));
        if(piso==actual){
        system("cls");
        cout<<"\n\n\tERROR, el piso que seleccionaste es el mismo en el que estas  \n"<<endl;
            usleep(2000);
            system("cls");
        }
       if (piso>actual){
           system("cls");
           for(i=actual;i<=piso;i++)
            {
              cout<<endl<<endl<<"\t\t\tVoy Subiendo "<<endl<<endl<<"\t\t\t| Piso: ["<<i<<"] |"<<endl<<"\a";
              usleep(2000);
              system("cls");
             }
           confirmacion();
            actual=i-1;
               usleep(4000);
            system("cls");
           }
       if (piso<actual)
           {
            system("cls");
            for(i=actual;i>=piso;i--){
           cout<<" \n\n\t\t\tVoy Bajando "<<endl;
           cout<<endl<<"\t\t\t| Piso: ["<<i<<"] |"<<endl;
                 //cout<<"\a";
                 usleep(2000);
              system("cls");
             }
                confirmacion();
             actual=i+1;
             usleep(4000);
             system("cls");
            }
        direccion();
        cout<<endl;
}


int main() {
    elevador *boton;
    boton=new elevador;
    boton->direccion();
//5    getch();
    delete boton;
}

